//Primero empiezo con las variables

var tiempo=[];
var tiemp=0;
var salir="";
var i=0;
var mejorResultado=1000;
var peorResultado=0;
var mediaResultado=0;

//Por penultimo aunque realmente vaya el segundo funciones

function mejRes(t) {
  for (let i = 0; i < t.length; i++) {
    if (t[i]<mejorResultado) {
      mejorResultado=t[i];
    }
  }
  console.log("La mejor vuelta son: "+mejorResultado+" segundos");
}

function peoRes(t) {
  for (let i = 0; i < t.length; i++) {
  if (t[i]>peorResultado) {
    peorResultado=t[i];
    }
  }
  console.log("La peor vuelta son: "+peorResultado+" segundos");
}

function medRes(t) {
  for (let i = 0; i < t.length; i++) {
  mediaResultado=mediaResultado+t[i];
  mediaResultado=mediaResultado/t.length;
  mediaResultado=mediaResultado.toFixed(3)
  }
  console.log("La media de vueltas es: "+mediaResultado+" segundos");
}

//Segundo es hacer la pregunta en bucle pidiendo tiempo de vueltas

do {
  tiemp=Number(prompt("Dame el tiempo de esta vuelta en segundos"));
  if (tiemp>=1 && tiemp<mejorResultado) {
    tiempo.push(tiemp);
    salir=prompt("Si quieres terminar escribe salir, si prefieres seguir pulsa enter");
  }
} while (salir!="salir");

//Y finalmente llamo a las funciones para que hagan lo que he pedido

mejRes(tiempo);
peoRes(tiempo);
medRes(tiempo);
